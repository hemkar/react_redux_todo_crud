const initialState = {
  data: "todo from reducer. No data found",
  todoData: [],
  deletedToDo: [],
  isEditing: false,
  selectedIndex: "",
  selectedValue: { id: "", title: "" }
};

const todoReducer = (state = initialState, action) => {
  console.log("inside reducer", state);
  const { type, payload } = action;

  switch (type) {
    case "CHANGE_STATE": {
      const { data } = payload;
      return { ...state, data };
    }

    case "INIT_TODO": {
      console.log(payload);
      const { todoData } = payload;
      console.log("length of todo data ", todoData.length);
      return { ...state, todoData };
    }

    case "ENABLE_EDIT": {
      console.log(payload);
      const { selectedIndex } = payload;
      const { todoData } = state;
      let selectedValue = todoData[selectedIndex];
      console.log(
        "selectedIndex ",
        selectedIndex,
        " selectedValue ",
        selectedValue
      );
      return { ...state, isEditing: true, selectedIndex, selectedValue };
    }

    case "CLOSE_EDIT_TODO": {
      console.log("inside CLOSE_EDIT_TODO");
      const isEditing = false;
      return { ...state, isEditing };
    }

    case "UPDATE_TODO": {
      console.log("inside UPDATE_TODO");
      const { selectedIndex, selectedValue, todoData } = state;

      const updatedToDo = [
        ...todoData.slice(0, parseInt(selectedIndex)),
        selectedValue,
        ...todoData.slice(parseInt(selectedIndex) + 1)
      ];

      console.log(updatedToDo);
      return {
        ...state,
        todoData: updatedToDo,
        selectedIndex: -1,
        selectedValue: {},
        isEditing: false
      };
    }
    case "UPDATE_SELECTED_TODO": {
      const { name, value } = payload;
      console.log(name, value);

      const selectedValue = { ...state.selectedValue, [name]: value };
      return { ...state, selectedValue };
    }

    // is it ok to use dispatch for onchange during adding a new entry  
    // case "NEW_TODO": {
    //   const { name, value } = payload;
    //   const selectedValue = { ...state.selectedValue, [name]: value };
    //   return { ...state, selectedValue };
    // }
    
    case "ADD_NEW_TODO": {
      const {newToDo}=payload;
      const newTodoData=[...state.todoData,newToDo]
      return {...state,todoData:newTodoData}
    }
    case "HANDLE_COMPLETE": {
      console.log("inside handle Complete");
      const { selectedIndex } = payload;
      const {todoData,deletedToDo}=state;

      const updatedToDo = [
        ...todoData.slice(0,parseInt(selectedIndex)),
        ...todoData.slice(parseInt(selectedIndex) + 1)
      ];

      const delTodo=[state.todoData[selectedIndex],...deletedToDo];
      console.log('delTodo ',delTodo);
      return {...state, todoData:updatedToDo,deletedToDo:delTodo}
    }


    case "HANDLE_DELETE": {
      console.log("inside handle Delete");
      const { selectedIndex } = payload;
      const {todoData}=state;

      const updatedToDo = [
        ...todoData.slice(0,parseInt(selectedIndex)),
        ...todoData.slice(parseInt(selectedIndex) + 1)
      ];
      return {...state, todoData:updatedToDo}
    }


    case "HANDLE_COMPLETE_DELETE": {
      console.log("inside handle Delete");
      const { selectedIndex } = payload;
      const {deletedToDo}=state;

      const updatedToDo = [
        ...deletedToDo.slice(0,parseInt(selectedIndex)),
        ...deletedToDo.slice(parseInt(selectedIndex) + 1)
      ];
      return {...state, deletedToDo:updatedToDo}
    }


    default: {
      return state;
    }
  }
};
export default todoReducer;
