import React, { Component, Fragment } from "react";
import TodoComponent from "./todo/TodoComponent";
import "./App.css";
import { connect } from "react-redux";

class App extends Component {
  

  handleEdit = (e, selectedIndex) => {
    e.stopPropagation();
    this.props.dispatch({type:'ENABLE_EDIT', payload:{selectedIndex}});
  }

  closePopup =  ()=>{
    this.props.dispatch({type: 'CLOSE_EDIT_TODO'})
  }

  handleSubmitEditToDO = () => {
    const {props:{selectedValue:{id,title}={}}}= this;

    if(!id || !title){
      alert("please provide required details");
      return;
    }

    this.props.dispatch({type:'UPDATE_TODO'});
  }

  handleEditToDoInput =(e,type) =>{
    console.log('inside handleEditToDoInput');
    console.log("event logged handleEditToDoInput ",e.target);
    const{target :{name,value}}=e;
    this.props.dispatch({type:'UPDATE_SELECTED_TODO',payload:{name,value}});
  }
  
  handleAddToDo =(id,title) => {
    debugger;
    if(!title){
      alert("Add task title ");
      return;
    }
    const newToDo={id,title};
    this.props.dispatch({type:"ADD_NEW_TODO",payload:{newToDo}});
  }


  handleDelete = (e,selectedIndex)=>{
    console.log('Inside handleDelete');
    this.props.dispatch({type:'HANDLE_DELETE', payload:{selectedIndex}})
  }

  handleComplete=(e,selectedIndex)=>{
    console.log('Inside handleComplete');
    this.props.dispatch({type:'HANDLE_COMPLETE', payload:{selectedIndex}})
  }

  handleCompleteDelete = (e,selectedIndex)=>{
    console.log('Inside handleCompleteDelete');
    this.props.dispatch({type:'HANDLE_COMPLETE_DELETE', payload:{selectedIndex}})
  }

  render() {
    const {
      props: { todoData = [], isEditing, selectedValue }
    } = this;
    return (
      <Fragment>
      <TodoComponent
        handleEdit={this.handleEdit}
        todoData={todoData}
        isEditing={isEditing}
        selectedValue={selectedValue}
        closePopup={this.closePopup}
        handleSubmitEditToDO={this.handleSubmitEditToDO}
        handleEditToDoInput={this.handleEditToDoInput}
        //handleInputChanges={this.handleInputChanges}
        handleDelete={this.handleDelete}
        handleAddToDo ={this.handleAddToDo}
        handleComplete={this.handleComplete}
        handleCompleteDelete={this.handleCompleteDelete}
      />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  todoReducer: {
    todoData,
    selectedValue = {},
    isEditing = false,
    selectedIndex = -1
  } = {}
} = {}) => ({ todoData, selectedValue, isEditing, selectedIndex });

export default connect(mapStateToProps)(App);
