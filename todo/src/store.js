import {createStore } from 'redux';
import {composeWithDevTools} from  'redux-devtools-extension';
import reducer from './reducer'

const composeEnhancer= composeWithDevTools({});
const store = createStore(reducer,composeEnhancer());
store.subscribe(() => (console.log('store subscribed')));

export default store;
