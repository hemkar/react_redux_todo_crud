import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import "./todo.css";
import "./Popup.css"
import "./Dropdown.css"
import deleteSvg from "../assets/images/delete.png";
import editSvg from "../assets/images/edit.png";
import completeSvg from "../assets/images/complete.png";

class TodoComponent extends Component {
  
  constructor(props) {
    super(props);
    this.state={
      inputId:"",
      inputTitle:""
    };
  }


  

  showDropDown =() => {
    document.getElementById("myDropdown").classList.toggle("show");
  }

   handleInputChanges = (e,type) =>{
     if(!e.target.value){
       return;
     }
     console.log("event logged handleInputChanges ",e.target.name, " : ",e.target.value);
     this.setState({[e.target.name]:e.target.value});
   }

  componentDidMount() {
    console.log("inside component did mount");

    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => response.json())
      .then(todoData => {
        todoData=todoData.slice(0,5);
        if (todoData.length) {
          console.log(todoData);
          this.props.dispatch({
            type: "INIT_TODO",
            payload: { todoData}
          });
        }
      });
  }

  
  changeState = () => {
    const data = "Data changed through redux";
    const toggle = this.props.toggle;
    this.props.dispatch({ type: "CHANGE_STATE", payload: { data, toggle } });
  };

  render() {
    console.log("Inside render method", this.props.todoData);

    const {
      props: { todoData = [], isEditing, selectedValue,deletedToDo},state :{inputId,inputTitle}
          } = this;

    const todoArray = todoData;
    const delToDoArr=deletedToDo;
    return (
      <Fragment>
        <div className="float">
          <div className="todo textColor">
            <h1>Active tasks</h1>
            <input
              placeholder="title"
              name="inputTitle"
              value={inputTitle}
              onChange={e => this.handleInputChanges(e, "title")} // is it good idea to use dispatch here
            />

            <button
              onClick={() => {
                this.props.handleAddToDo(inputId, inputTitle);
                this.setState({ inputTitle: "", inputId: "" }); // is it good idea to use dispatch here
              }}
            >
              Add
            </button>
          </div>

          {todoArray.map((todo, index) => {
            return (
              <div className="todo" key={index}>
                <div className="dropdown  floatright">
                <button onClick={this.showDropDown} title="Options" className="dropbtn">...</button>
                <div id="myDropdown" className="dropdown-content">
                <img
                className="floatright"
                  src={deleteSvg}
                  alt="delete"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleDelete(e, index)}
                />
                <img
                 className="floatright"
                  src={editSvg}
                  alt="edit"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleEdit(e, index)}
                />
                   <img
                className="floatright"
                  src={completeSvg}
                  alt="delete"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleComplete(e, index)}
                />
                 </div>
                </div> 
                {/* <img
                className="floatright"
                  src={deleteSvg}
                  alt="delete"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleDelete(e, index)}
                /> */}

                {/* <img
                 className="floatright"
                  src={editSvg}
                  alt="edit"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleEdit(e, index)}
                /> */}

                {/* <img
                className="floatright"
                  src={completeSvg}
                  alt="delete"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleComplete(e, index)}
                /> */}
                
                <h6>{todo.title}</h6>
              </div>
            );
          })}
        </div>

        <div className="float">
          <h1 className="todo textColor">Completed tasks</h1>
          {delToDoArr.length ? (
            delToDoArr.map((delTodo, index) => {
              return (
                <div className="todo" key={index}>
                <img
                className="floatright"
                  src={deleteSvg}
                  alt="delete"
                  width="25px"
                  name={index}
                  onClick={e => this.props.handleCompleteDelete(e, index)}
                />
                  <h6>{delTodo.title}</h6>
                </div>
              );
            })
          ) : (
            <div className="todo warningColor">
              <h5>No Task in Complete status</h5>
            </div>
          )}
        </div>

        {isEditing && (
          <div className="popup">
            <div className="popupToDoData">
              <Fragment>
                <div>
                  <input
                    type="text"
                    value={selectedValue.title}
                    name="title"
                    onChange={e =>
                      this.props.handleEditToDoInput(e, "title")
                    }
                  />
                </div>
                <div>
                  <button onClick={this.props.handleSubmitEditToDO}>
                    Done
                  </button>
                </div>
              </Fragment>
            </div>
            <button className="closePopup" onClick={this.props.closePopup}>
              Close
            </button>
          </div>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const data = state.todoReducer.data;
  const toggle = state.todoReducer.toggle;
  const todoData = state.todoReducer.todoData;
  const deletedToDo=state.todoReducer.deletedToDo;
  return { data, toggle, todoData,deletedToDo };
};

export default connect(mapStateToProps)(TodoComponent);
